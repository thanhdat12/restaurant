��    1      �      ,      ,  @   -     n     z     �     �  
   �     �     �     �     �     �                     ,     9     O     h     n     �     �     �     �     �     �     �                     &     +     =     L     [  ,   k     �     �     �     �     �       	                	   ,  
   6     A     V    c  q   z     �     �     	     	  	   &	  
   0	      ;	     \	     j	     r	     �	     �	     �	  
   �	  
   �	     �	     �	     �	     �	     �	     �	     
     
     1
     F
     N
     b
     i
  
   u
     �
     �
     �
     �
  D   �
                ;  2   X  2   �     �     �     �     �     �     �     �        %s has been added to your cart. %s have been added to your cart. Add to cart Additional information Apply coupon Billing details Categories Categories: Category: Categories: Change address Checkout Choose an option Coupon code Default sorting Description Local pickup Local pickup (legacy) No products in the cart. Price Proceed to Checkout Proceed to Checkout button Proceed to checkout Product Product Categories Product Categories List Product categories Quantity Related products Reviews Reviews (%d) Sale Select a category Select options Shipping to %s Shipping to %s. Showing all %d result Showing all %d results Sort by average rating Sort by latest Sort by popularity Sort by price: high to low Sort by price: low to high Subtotal Subtotal: Total Update cart View cart Your order default-slugproduct slugproduct Project-Id-Version: WooCommerce 4.5.2
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/woocommerce
Last-Translator: 
Language-Team: Polski
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2020-09-14T17:19:58+00:00
PO-Revision-Date: 2020-09-18 12:52+0000
X-Generator: Loco https://localise.biz/
X-Domain: woocommerce
Language: pl_PL
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10 >= 2 && n%10<=4 &&(n%100<10||n%100 >= 20)? 1 : 2);
X-Loco-Version: 2.4.3; wp-5.5.1 %s Został Dodany Do Twojego Koszyka. %s Został Dodany Do Twojego Koszyka. %s Został Dodany Do Twojego Koszyka. Dodaj Dodatkowe informacje Zapisz Szczegóły Płatności Kategorie Kategorie: Kategorie: Kategorie: Kategorie: Zmiana Adresu Do kasy Wybierz opcję Tu wpisz kod rabatowy Domyślne sortowanie Opis Na miejscu Na Miejscu Brak produktów w koszyku. Cena Do Kasy Do Kasy Do kasy Produkt Kategorie produktów Kategorie produktów Kategorie produktów Ilość Produkty Powiązane Opinie Opinie (%d) wyprzedaż Wybierz kategorię Zamów Twój adress %s. Twój adress %s. Pokazując Wszystko %d Pokazując Wszystko %d Pokazując Wszystko %d Sortuj Według Średniej Oceny Sortuj Według Najnowszych Sortuj Według Popularności Sortuj Według Ceny: Od Najwyższej Do Najniższej Sortuj Według Ceny: Od Najniższej do Najwyższej Suma Suma Suma Zaktualizuj Koszyk Koszyk Twoje Zamówienie Produkt Produkt 