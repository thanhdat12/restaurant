# ChangeLog

##[1.0.7]  - 2020-06-26
- added refunds from admin panel

##[1.0.6]  - 2020-05-18
- fix order cancellation
- add automatic cancellation to order originated by administrators

##[1.0.5]  - 2020-05-05
- New order summary mail after order created with settings in admin
- Changed email with payment notification - added validation for order status

##[1.0.4]  - 2020-04-22
- minor fixes

##[1.0.3]  - 2020-04-15
- minor fixes

##[1.0.2]  - 2020-04-03
- change title of mail for unpaid order

##[1.0.1]  - 2020-03-30
- Statistical section integrated

##[1.0.0]  - 2020-03-20
- migration to Woo 4.x without satistic section
