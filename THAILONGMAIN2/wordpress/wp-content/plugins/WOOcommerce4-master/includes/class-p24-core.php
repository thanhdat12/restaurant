<?php
/**
 * File that define P24_Core class.
 *
 * @package Przelewy24
 */

defined( 'ABSPATH' ) || exit;

/**
 * Core methods for Przelewy 24 plugin.
 */
class P24_Core {

	/**
	 * String to add to loaded scripts.
	 *
	 * @var string
	 */
	const SCRIPTS_VERSION = '1.0.0';

	/**
	 * Key used to mark when Przelewy24 has been used to pay for order.
	 *
	 * @var string
	 */
	const CHOSEN_TIMESTAMP_META_KEY = '_p24_chosen_timestamp';

    /**
     * Key used to store session id, which may be used later i.e. for refunds
     */
	const ORDER_SESSION_ID_KEY = '_p24_order_session_id';

	/**
	 * The null or P24_Multi_Currency instance.
	 *
	 * @var null|P24_Multi_Currency
	 */
	private $multi_currency = null;

	/**
	 * The P24_Request_Support instance.
	 *
	 * @var P24_Request_Support
	 */
	private $request_support;

	/**
	 * The P24_Config_Menu instance.
	 *
	 * @var P24_Config_Menu;
	 */
	private $config_menu;

	/**
	 * The instance of class configuring WP menu.
	 *
	 * @var P24_Multi_Currency_Menu
	 */
	private $wp_menu_support;

	/**
	 * The WC_Gateway_Przelewy24 instance.
	 *
	 * @var WC_Gateway_Przelewy24
	 */
	private $gateway;

	/**
	 * Context provider.
	 *
	 * @var P24_Context_Provider
	 */
	private $context_provider;

	/**
	 * Construct class instance.
	 */
	public function __construct() {
		$this->request_support  = new P24_Request_Support();
		$this->context_provider = new P24_Context_Provider();
		$this->wp_menu_support  = new P24_Multi_Currency_Menu( $this );
		if ( ! $this->is_in_user_mode() ) {
			$this->config_menu = new P24_Config_Menu( $this );
		}
	}

	/**
	 * Check if page is in user mode.
	 *
	 * @return bool
	 */
	public function is_in_user_mode() {
		if ( is_admin() ) {
			return false;
		} elseif ( defined( 'DOING_CRON' ) && DOING_CRON ) {
			return false;
		} elseif (  $this->is_in_json_mode() ) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Check if application is in JSON mode.
	 *
	 * @return bool
	 */
	public function is_in_json_mode() {
		return (bool) preg_match( '/^\\/wp\\-json\\//', $_SERVER['REQUEST_URI'] );
	}

	/**
	 * Check if internal multi currency is activated.
	 *
	 * @return bool
	 */
	public function is_multi_currency_active() {
		return (bool) $this->multi_currency;
	}

	/**
	 * Return multi currency instance.
	 *
	 * Should be called only if multi currency i active.
	 *
	 * @return P24_Multi_Currency
	 * @throws LogicException If there is no instance.
	 */
	public function get_multi_currency_instance() {
		if ( $this->is_multi_currency_active() ) {
			return $this->multi_currency;
		} else {
			throw new LogicException( 'Multi currency is not active. It should be tested.' );
		}
	}

	/**
	 * Try override active currency.
	 *
	 * The communication with Przelewy24 is quite late.
	 *
	 * @param P24_Communication_Parser $parser The P24_Communication_Parser instance.
	 */
	public function try_override_active_currency( P24_Communication_Parser $parser ) {
		if ( $this->is_multi_currency_active() ) {
			$this->multi_currency->try_override_active_currency( $parser );
		}
	}

	/**
	 * Return current instance i parameter is null.
	 *
	 * This should be useful in filters.
	 *
	 * @param mixed $default Default value from filter.
	 * @return P24_Core
	 */
	public function get_this_if_null( $default ) {
		return $default ?: $this;
	}

	/**
	 * Render template and output.
	 *
	 * @param string $template The name of template.
	 * @param array  $params The array of parameters.
	 * @throws LogicException If the file is not found.
	 */
	public function render_template( $template, $params = [] ) {
		$dir  = __DIR__ . '/../templates/';
		$file = $template . '.php';
		wc_get_template( $file, $params, $dir, $dir );
	}

	/**
	 * Check if multi currency should be activated.
     *
     * @return bool
	 */
	public function should_activate_multi_currency() {
		$common = get_option( P24_Request_Support::OPTION_KEY_COMMON, [] );

		return array_key_exists( 'p24_multi_currency', $common ) && 'yes' === $common['p24_multi_currency'];
	}

	/**
	 * Check if order created notification should be activated.
     *
     * @return bool
	 */
	public function should_activate_order_created_notification() {
		$common = get_option( P24_Request_Support::OPTION_KEY_COMMON, [] );

		return array_key_exists( 'p24_notification_order_created', $common ) && 'yes' === $common['p24_notification_order_created'];
	}

	/**
	 * Register gateway.
	 *
	 * The constructor of gateway has to be called in external plugin.
	 *
	 * @param WC_Gateway_Przelewy24 $gateway The gateway instance.
	 */
	public function register_gateway( WC_Gateway_Przelewy24 $gateway ) {
		$this->gateway = $gateway;
	}

	/**
	 * Get config for currency.
	 *
	 * @param null|string $currency The currency for requested config.
	 * @return P24_Config_Accessor
	 * @throws LogicException If there is no gateway created.
	 */
	public function get_config_for_currency( $currency = null ) {
		if ( ! $this->gateway ) {
			throw new LogicException( 'Gateway in not registered yet.' );
		}
		return $this->gateway->load_settings_from_db_formatted( $currency );
	}

	/**
	 * Get P24_Message_Validator instance.
	 *
	 * @return P24_Message_Validator
	 */
	public function get_message_validator() {
		return new P24_Message_Validator();
	}

	/**
	 * Get P24_Communication_Parser instance.
	 *
	 * @return P24_Communication_Parser
	 */
	public function get_communication_parser() {
		$message_validator = $this->get_message_validator();
		return new P24_Communication_Parser( $message_validator );
	}

	/**
	 * Get default currency.
	 *
	 * @return string
	 */
	public function get_default_currency() {
		if ( $this->is_multi_currency_active() ) {
			return $this->multi_currency->get_default_currency();
		} else {
			return get_woocommerce_currency();
		}
	}

	/**
	 * Late configuration after Woocommerce init.
	 */
	public function after_woocommerce_init() {
		$this->request_support->analyse();
		$this->request_support->flush_options();
		if ( $this->should_activate_multi_currency() ) {
			/* The logic to set active currency is in P24_Multi_Currency class. */
			$currency_changes     = $this->request_support->get_currency_changes();
			$this->multi_currency = new P24_Multi_Currency( $this, $currency_changes );
			$this->multi_currency->bind_events();
			if ( is_admin() ) {
				$this->config_menu->bind_multi_currency_events();
			}
		}
	}

	/**
	 * Check if order is cancellable.
	 *
	 * @param bool     $default Default value.
	 * @param WC_Order $order The order to check.
	 *
	 * @return mixed
	 */
	public function check_if_cancellable( $default, $order ) {
		if ( $default ) {
			return $default;
		} elseif ( ! $order instanceof WC_Order ) {
			return $default;
		} elseif ( WC_Gateway_Przelewy24::PAYMENT_METHOD === $order->get_payment_method() ) {
			$minutes   = (int) get_option( P24_Woo_Commerce_Internals::HOLD_STOCK_MINUTES );
			$timestamp = $order->get_meta( self::CHOSEN_TIMESTAMP_META_KEY );
			if ( $minutes && $timestamp ) {
				$now     = time();
				$seconds = $minutes * 60;

				return ( $timestamp + $seconds ) < $now;
			}
		}

		return $default;
	}

	/**
	 * Bind events.
	 */
	public function bind_core_events() {
		add_filter( 'przelewy24_plugin_instance', [ $this, 'get_this_if_null' ] );
		add_action( 'woocommerce_init', [ $this, 'after_woocommerce_init' ] );
		add_filter( 'woocommerce_cancel_unpaid_order', [ $this, 'check_if_cancellable' ], 10, 2 );
		$this->wp_menu_support->bind_events();
		if ( is_admin() ) {
			$this->config_menu->bind_common_events();
		}
	}

	/**
	 * Get context provider.
	 */
	public function get_context_provider() {
		return $this->context_provider;
	}

	/**
	 * Converts money amount from stringified float (1.10) to cents in int.
	 *
	 * @param string $string Stringified float.
	 *
	 * @return int
	 */
	public static function convert_stringified_float_to_cents( $string ) {
		list( $fulls, $cents ) = explode( '.', $string );

		$sum = (int) $fulls * 100;

		if ( $cents ) {
			$sum += $cents;
		}

		return $sum;
	}
}
