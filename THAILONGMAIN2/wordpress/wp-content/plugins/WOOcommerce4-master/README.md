﻿=== WooCommerce4 Przelewy24 Payment Gateway ===
Contributors: przelewy24
Donate link: http://www.przelewy24.pl
Tags: woocommerce, Przelewy24, payment, payment gateway, platnosci
Requires at least: 4.0.0
Tested up to: 4.0.0
Stable tag: 1.0.6
License: GPLv3

== Description ==

Przelewy24 Payment Gateway supports:

* Polish online transfers and installments

= Features =

The Przelewy24 Payment Gateway for WooCommerce adds the Przelewy24 payment option and enables you to process the following operations in your shop:

* Creating a payment order
* Updating order status (canceling/completing an order will simultaneously update payment's status)

= Usage =

Przelewy24 Payment Gateway is visible for your customers as a single "Buy and Pay" button during checkout. After clicking the button customer is redirected to the Payment Summary page to choose payment method. After successful payment customer is redirected back to your shop.

== Installation ==

If you do not already have Przelewy24 merchant account [please register](https://www.przelewy24.pl/rejestracja).

In the Wordpress administration panel:

1. Go to **WooCommerce** -> **Settings section**
1. Choose **Checkout** tab and scroll down to the **"Payment Gateways"** section
1. Choose **Settings** option next to the **Przelewy24** name
1. Enable and configure the plugin


== Changelog ==

##[1.0.6]  - 2020-05-18
- fix order cancellation
- add automatic cancellation to order originated by administrators

##[1.0.5]  - 2020-05-05
- New order summary mail after order created with settings in admin
- Changed email with payment notification - added validation for order status

##[1.0.4]  - 2020-04-22
- minor fixes

##[1.0.3]  - 2020-04-15
- minor fixes

##[1.0.2]  - 2020-04-03
- change title of mail for unpaid order

##[1.0.1]  - 2020-03-30
- Statistical section integrated

##[1.0.0]  - 2020-03-20
- migration to Woo 4.x without satistic section
