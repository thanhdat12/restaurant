<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'thailongmain' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '4R%3y%1TMi: 1,FwNsR>;iAOMX-FY<;4K1s]G)9Vb?A7z*X]/Cj`/Xz=1Gb;L][&' );
define( 'SECURE_AUTH_KEY',  'Xr~0G?;E|VsOTFR[L8&HlbNu~ CK%6FT]Ym-%C#t^-!v}ypR(!-t`Ji5Z ]~{1m}' );
define( 'LOGGED_IN_KEY',    'QlQ~VHS1K1): {<qPw_3&q6*&,-re?&TyT.Pb+PYf)8R~`Z`L-.Jrjk 9HSkO{U8' );
define( 'NONCE_KEY',        '9PDtx?4s0f)JkO}?upB).#:lykiu9Z?YlkiuQ?ci%|3#> TJG(}o%PvB.Wq2>kSx' );
define( 'AUTH_SALT',        'x@R+ae/F;ZSbK3Osc*|F@re]{z6bQ.:%IH@-G$&Inr]~%UhiUTs!w)A.1~GZ<ts2' );
define( 'SECURE_AUTH_SALT', 'sTZ0])=nzWg?|FeL;vITsUeO)aPMZaivQMgXa?rY-%oOZHJ6}j!XU,Hk7ORD@;=f' );
define( 'LOGGED_IN_SALT',   'J:=<:ySQyhTWk;A01h[n-*=CMr:4{d0O1kf/;`<5/ }]lUze~?&pY7XzGrTa?oV]' );
define( 'NONCE_SALT',       ':NGs?5C>Iv|X)p~,;2&)JgV93eAq:Dz~a4S-quxsT}48w2?J OYX68BcCX#-Z4mP' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
